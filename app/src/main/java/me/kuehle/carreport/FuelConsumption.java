/*
 * Copyright 2013 Mihai Ibanescu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.kuehle.carreport;

import android.content.Context;

public class FuelConsumption {
    private Type consumptionType;
    private String typeCategory;
    private String unitVolume;
    private String unitDistance;
    private Preferences preferences;
    private Context context;

    public FuelConsumption(Context context) {
        this.preferences = new Preferences(context);
        this.context = context;
        this.typeCategory = "";
        reload();
    }

    public static Type findConsumptionType(int id) {
        if (id == Type.DIST_FOR_VOL.id)
            return Type.DIST_FOR_VOL;
        return Type.VOL_FOR_DIST;
    }

    public void reload() {
        int id = preferences.getUnitFuelConsumption();
        this.setConsumptionType(id);
        this.unitVolume = preferences.getUnitVolume();
        this.unitDistance = preferences.getUnitDistance();
    }

    public void setConsumptionType(int id) { this.consumptionType = FuelConsumption.findConsumptionType(id); }

    public void setTypeCategory(String typeCategory)    { this.typeCategory = typeCategory; }

    public void setUnitVolume(String unitVolume) { this.unitVolume = unitVolume; }

    public void setUnitDistance(String unitDistance) {
        this.unitDistance = unitDistance;
    }

    public double getConversionRatio()   {
        String[] fuelTypes = context.getResources().getStringArray(R.array.fuel_type_array);
        if (this.typeCategory.equals(fuelTypes[10])) {
            if (this.unitVolume.toUpperCase().startsWith("L")) {
                return 3.7854 / 33.7;
            } else {
                return 1.0 / 33.7;
            }
        } else if (this.typeCategory.equals(fuelTypes[1]))   {
            return 37.95 / 34.02;
        } else if (this.typeCategory.equals(fuelTypes[2]))   {
            return 35.04 / 34.02;
        } else if (this.typeCategory.equals(fuelTypes[3]))   {
            return 22.37 / 34.02;
        } else if (this.typeCategory.equals(fuelTypes[4]))   {
            return 24.03 / 34.02;
        } else if (this.typeCategory.equals(fuelTypes[5]))   {
            return 28.81 / 34.02;    // Note that volume doesn't really make sense here
        } else if (this.typeCategory.equals(fuelTypes[6]))   {
            return 8.47 / 34.02;    // Note that volume doesn't really make sense here
        } else if (this.typeCategory.equals(fuelTypes[7]))   {
            return 8.94 / 34.02;
        } else if (this.typeCategory.equals(fuelTypes[8]))   {
            return 24.9 / 34.02;
        } else if (this.typeCategory.equals(fuelTypes[9]))   {
            return 16.78 / 34.02;
        }
        return 1; // also handles gasoline
    }

    public float computeFuelConsumption(Type consumptionType, float volume, float distance) {
        if (consumptionType == Type.DIST_FOR_VOL) {
            return (float) (distance / (volume * this.getConversionRatio()));
        } else {
            return (float) (100.0 * volume * this.getConversionRatio() / distance);
        }
    }

    public float computeRawFuelConsumption(Type consumptionType, float volume, float distance) {
        if (consumptionType == Type.DIST_FOR_VOL) {
            return distance / volume;
        } else {
            return (float) (100.0 * volume / distance);
        }
    }

    public float computeFuelConsumption(float volume, float distance) {
        return computeFuelConsumption(this.consumptionType, volume, distance);
    }

    public String getUnitLabel(Type consumptionType) {
        String[] fuelTypes = context.getResources().getStringArray(R.array.fuel_type_array);
        if (consumptionType == Type.DIST_FOR_VOL) {
            if (this.typeCategory.equals(fuelTypes[10])) {
                return String.format("%s/kWh", this.unitDistance);
            }
            return String.format("%s/%s", this.unitDistance, this.unitVolume);
        } else {
            if (this.typeCategory.equals(fuelTypes[10])) {
                return String.format("kWh/100%s", this.unitDistance);
            }
            return String.format("%s/100%s", this.unitVolume, this.unitDistance);
        }
    }

    public String getUnitLabel() {
        return this.getUnitLabel(this.consumptionType);
    }

    public String[] getUnitsEntries() {
        String[] list = new String[2];
        list[0] = this.getUnitLabel(Type.VOL_FOR_DIST);
        list[1] = this.getUnitLabel(Type.DIST_FOR_VOL);
        return list;
    }

    public String[] getUnitsEntryValues() {
        String[] list = new String[2];
        list[0] = String.valueOf(Type.VOL_FOR_DIST.id);
        list[1] = String.valueOf(Type.DIST_FOR_VOL.id);
        return list;
    }

    public enum Type {
        VOL_FOR_DIST(0), DIST_FOR_VOL(1);

        public final int id;

        Type(int id) {
            this.id = id;
        }
    }
}
